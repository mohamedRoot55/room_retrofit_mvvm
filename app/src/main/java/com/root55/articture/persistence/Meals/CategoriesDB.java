package com.root55.articture.persistence.Meals;

import android.app.Application;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.root55.articture.Entites.Category;

@Database(entities = {Category.class}, version = 1)
public abstract class CategoriesDB extends RoomDatabase {


    public abstract CategoriesDao categoriesDao();

    private static final String DATABASE_NAME = "categories_db";

    private static CategoriesDB categoriesDB = null;

    public static CategoriesDB getInstance(Context context) {
        if (categoriesDB == null) {
            categoriesDB = Room.databaseBuilder(
                    context.getApplicationContext(),
                    CategoriesDB.class, DATABASE_NAME)
                    .build();
        }
        return categoriesDB;
    }
}
