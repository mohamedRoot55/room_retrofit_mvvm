package com.root55.articture.persistence.Meals;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.root55.articture.Entites.Category;

import java.util.List;

import static androidx.room.OnConflictStrategy.IGNORE;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CategoriesDao {

    @Query("SELECT * FROM category")
    LiveData<List<Category>> getAllCategories() ;


    @Insert(onConflict = IGNORE)
    long[] InsertCategories(Category ... categories);

    @Update(onConflict = REPLACE)
    void UpdateCategories(Category category) ;

}
