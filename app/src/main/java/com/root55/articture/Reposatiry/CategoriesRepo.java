package com.root55.articture.Reposatiry;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.root55.articture.AppExecutors;
import com.root55.articture.Entites.Category;
import com.root55.articture.Utils.NetworkBoundResource;
import com.root55.articture.Utils.Resource;
import com.root55.articture.WepService.WepServiceClient;
import com.root55.articture.WepService.responses.ApiResponse;
import com.root55.articture.WepService.responses.CategoryResponse;
import com.root55.articture.persistence.Meals.CategoriesDB;
import com.root55.articture.persistence.Meals.CategoriesDao;

import java.util.List;


public class CategoriesRepo {
    private  CategoriesDao categoriesDao;
    private static CategoriesRepo categoriesRepo = null;


    public CategoriesRepo(Context context) {
        categoriesDao = CategoriesDB.getInstance(context).categoriesDao();
    }

    public static CategoriesRepo getInstance(Application application) {
        if (categoriesRepo == null) {
            categoriesRepo = new CategoriesRepo(application);
        }
        return categoriesRepo;
    }


    public LiveData<Resource<List<Category>>> getCategoriesApi() {
        return new NetworkBoundResource<List<Category>, CategoryResponse>(AppExecutors.getInstance()) {
            @Override
            protected void saveCallResult(@NonNull CategoryResponse item) {
                if (item.getCategories() != null) {

                    Log.e("saveCallResult" , item.getCategories().get(1).getStrCategoryDescription()) ;
                    Category[] categories = new Category[item.getCategories().size()];
                    int index = 0;
                    for (long rowId : categoriesDao.InsertCategories(item.getCategories().toArray(categories))) {


                        if (rowId == -1) {

                            categoriesDao.UpdateCategories(categories[index]);
                        }
                        index++;
                    }
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Category> data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<Category>> loadFromDb() {
                return categoriesDao.getAllCategories();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<CategoryResponse>> createCall() {
              //  Log.e("call response" , WepServiceClient.getApiService().getCategories().getValue().toString()) ;
                return WepServiceClient.getRecipeApi().getCategories();
            }
        }.getAsLiveData();
    }
}
