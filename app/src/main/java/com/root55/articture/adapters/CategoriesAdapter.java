package com.root55.articture.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.root55.articture.Entites.Category;
import com.root55.articture.R;

import com.root55.articture.Utils.Constants;
import com.root55.articture.Utils.Resource;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {
    private List<Category> response;
     private Context context;
     private onItemClick onItemClick ;

    public CategoriesAdapter(List<Category> response , Context context , onItemClick onItemClick) {
        this.response = response;
      this.context = context;
      this.onItemClick = onItemClick ;
    }

    @NonNull
    @Override
    public CategoriesAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        CategoriesAdapter.CategoryViewHolder viewHolder = new CategoriesAdapter.CategoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoriesAdapter.CategoryViewHolder holder, int position) {

        final Category category = response.get(position) ;
        RequestOptions requestOptions = new RequestOptions() ;
        requestOptions.placeholder(Constants.getRandomDrawbleColor()).
                error(Constants.getRandomDrawbleColor()).
                diskCacheStrategy(DiskCacheStrategy.ALL) ;
        requestOptions.centerCrop() ;
        Glide.with(context).load(category.getStrCategoryThumb()).apply(requestOptions).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);

                return false;
            }
        }).transition(DrawableTransitionOptions.withCrossFade()).into(holder.imageView) ;
        holder.textView.setText(response.get(position).getStrCategory());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(category);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(response != null){
            return response.size();
        }
        return 0;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar ;
        ImageView imageView;
        TextView textView ;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar) ;
           imageView = itemView.findViewById(R.id.category_img);
            textView = itemView.findViewById(R.id.category_description) ;
        }
    }

public interface  onItemClick{
        public void onClick(Category category);
}
}

