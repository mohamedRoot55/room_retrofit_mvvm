package com.root55.articture.WepService;

import com.root55.articture.Utils.Constants;
import com.root55.articture.Utils.LiveDataCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.root55.articture.Utils.Constants.CONNECTION_TIMEOUT;
import static com.root55.articture.Utils.Constants.READ_TIMEOUT;
import static com.root55.articture.Utils.Constants.WRITE_TIMEOUT;

public class WepServiceClient {
//
//    private static Retrofit retrofit = null ;
//
//
//    private static OkHttpClient client = new OkHttpClient.Builder()
//
//            // establish connection to server
//            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
//
//            // time between each byte read from the server
//            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
//
//            // time between each byte sent to server
//            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
//
//            .retryOnConnectionFailure(false)
//
//            .build();
//    public static Retrofit getRetrofit(){
//        if(retrofit == null){
//            retrofit = new Retrofit.Builder().
//                    baseUrl(Constants.BASE_URL).
//                    client(client).
//                    addCallAdapterFactory(new LiveDataCallAdapterFactory()).
//                    addConverterFactory(GsonConverterFactory.create()).
//                    build() ;
//        }
//        return retrofit ;
//    }
//
//    private static ApiService apiService = getRetrofit().create(ApiService.class) ;
//
//    public static ApiService getApiService(){
//        return  apiService ;
//    }

    private static OkHttpClient client = new OkHttpClient.Builder()

            // establish connection to server
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)

            // time between each byte read from the server
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)

            // time between each byte sent to server
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)

            .retryOnConnectionFailure(false)

            .build();


    private static Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(client)
                    .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static ApiService apiService = retrofit.create(ApiService.class);

    public static ApiService getRecipeApi(){
        return apiService;
    }


}
