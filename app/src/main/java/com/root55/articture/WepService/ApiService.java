package com.root55.articture.WepService;

import androidx.lifecycle.LiveData;


import com.root55.articture.WepService.responses.ApiResponse;
import com.root55.articture.WepService.responses.CategoryResponse;


import retrofit2.http.GET;

public interface ApiService {


    @GET("categories.php")
    LiveData<ApiResponse<CategoryResponse>> getCategories() ;


}
