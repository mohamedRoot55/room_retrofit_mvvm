
package com.root55.articture.WepService.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.root55.articture.Entites.Category;

import java.util.List;

public class CategoryResponse {

    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}
