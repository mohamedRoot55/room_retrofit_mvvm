package com.root55.articture.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.root55.articture.Entites.Category;
import com.root55.articture.R;


public class RecipeDetails extends AppCompatActivity {

    ImageView mCat_Img;
    TextView mCat_Name, mCat_Description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        initViews();

        Intent intent = getIntent();
        Category category = intent.getParcelableExtra("category");

        assert category != null;
        Glide.with(this).load(category.getStrCategoryThumb()).into(mCat_Img);

        mCat_Name.setText(category.getStrCategory());
        mCat_Description.setText(category.getStrCategoryDescription());

    }

    private void initViews() {
        mCat_Img = findViewById(R.id.cat_img);
        mCat_Name = findViewById(R.id.cat_name);
        mCat_Description = findViewById(R.id.cat_description);
    }


}
