package com.root55.articture.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;

import com.root55.articture.Entites.Category;
import com.root55.articture.R;
import com.root55.articture.Utils.Resource;
import com.root55.articture.ViewModels.CategoryViewModel;
import com.root55.articture.adapters.CategoriesAdapter;


import java.util.List;


public class MainActivity extends AppCompatActivity implements CategoriesAdapter.onItemClick {


    CategoriesAdapter mCategories_adapter;
    RecyclerView.LayoutManager mCategoryManager;
    RecyclerView mCategory_Recycler;


    CategoryViewModel mCategory_ViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        SubscripEvents();
    }


    private void initViews() {
        mCategory_Recycler = findViewById(R.id.category_recycler);

        mCategoryManager = new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false);
        mCategory_ViewModel = ViewModelProviders.of(MainActivity.this).get(CategoryViewModel.class);


    }

    private void SubscripEvents() {
        mCategory_ViewModel.getCategoryApi().observe(this, new Observer<Resource<List<Category>>>() {
            @Override
            public void onChanged(Resource<List<Category>> listResource) {
                mCategories_adapter = new CategoriesAdapter(listResource.data, MainActivity.this, MainActivity.this);
                mCategory_Recycler.setAdapter(mCategories_adapter);
                mCategory_Recycler.setLayoutManager(mCategoryManager);
            }
        });
    }

    @Override
    public void onClick(Category category) {
        Intent intent = new Intent(MainActivity.this, RecipeDetails.class);
        intent.putExtra("category", category);
        startActivity(intent);
    }
}
