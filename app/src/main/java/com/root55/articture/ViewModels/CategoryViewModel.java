package com.root55.articture.ViewModels;


import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;


import com.root55.articture.Entites.Category;
import com.root55.articture.Reposatiry.CategoriesRepo;
import com.root55.articture.Utils.Resource;


import java.util.List;

public class CategoryViewModel extends AndroidViewModel {



    private CategoriesRepo categoriesRepo;


    public CategoryViewModel(@NonNull Application application) {
        super(application);
        categoriesRepo = CategoriesRepo.getInstance(application) ;
    }


    public LiveData<Resource<List<Category>>> getCategoryApi() {

        return categoriesRepo.getCategoriesApi();

    }

    }




